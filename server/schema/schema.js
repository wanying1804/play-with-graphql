const graphql = require('graphql');
const { GraphQLObjectType, GraphQLString, GraphQLSchema, GraphQLID, GraphQLInt, GraphQLList } = graphql;
const _ = require('lodash');

const books = [
    {name: 'abc', genre: 'Fantasy', id: '1', authorId: '1'},
    {name: 'efg', genre: 'Fantasy', id: '2', authorId: '2'},
    {name: 'him', genre: 'Sci-Fi', id: '3', authorId: '3'},
    {name: 'xxx', genre: 'Sci-Fi', id: '4', authorId: '1'},
    {name: 'bbb', genre: 'Sci-Fi', id: '5', authorId: '2'},
    {name: 'ccc', genre: 'Sci-Fi', id: '6', authorId: '3'},
];

const authors = [
    {name: 'cassie', age: 30, id: '1'},
    {name: 'tassie', age: 32, id: '2'},
    {name: 'xassie', age: 33, id: '3'},
]

const BookType = new GraphQLObjectType({
    name: 'Book',
    fields: () => ({
        id: {type: GraphQLID},
        name: {type: GraphQLString},
        genre: {type: GraphQLString},
        author: {
            type: AuthorType,
            resolve(parent, args) {
                return _.find(authors, {id: parent.authorId});
            }
        }
    })
});

const AuthorType = new GraphQLObjectType({
    name: 'Author',
    fields: () => ({
        id: {type: GraphQLID},
        name: {type: GraphQLString},
        age: {type: GraphQLInt},
        books: {
            type: new GraphQLList(BookType),
            resolve(parent, args) {
                return _.filter(books, {authorId: parent.id});
            }
        }
    })
});


const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        book: {
            type: BookType,
            args: {id: {type: GraphQLID}},
            resolve(parent, args) {
                //code to get data from db/other source
               return _.find(books, {id: args.id})
            }
        },
        author: {
            type: AuthorType,
            args: {id: {type: GraphQLID}},
            resolve(parent, args) {
                //code to get data from db/other source
                return _.find(authors, {id: args.id})
            }
        },
        books: {
            type: new GraphQLList(BookType),
            resolve(parent, args) {
                return books;
            }
        },
        authors: {
            type: new GraphQLList(AuthorType),
            resolve(parent, args) {
                return authors;
            }
        }
    }
});

module.exports = new GraphQLSchema({
    query: RootQuery
});

